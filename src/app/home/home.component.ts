import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss' ],
    animations: [ fadeInAnimation ]
})
export class HomeComponent {

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }
}
