import { Component, Input, OnDestroy } from '@angular/core';
import { Observable, OperatorFunction, Subscription, from, interval } from 'rxjs';
import { zip, switchMap } from 'rxjs/operators';

@Component({
    selector: 'rxt-operator-block',
    templateUrl: './operator-block.component.html',
    styleUrls: [ './operator-block.component.scss' ]
})
export class OperatorBlockComponent implements OnDestroy {
    // Member Variables ***************************************************************************
    private _sourceItems: any[];
    private _secondarySourceItems: any[];

    private _title: string;
    private _description: string;
    private _reference: string;
    private _action: string;

    private _function: OperatorFunction<any, any>;

    private _results: any[];

    private _subscription: Subscription;

    // Public Properties **************************************************************************
    @Input() public get sourceItems(): any[] {
        return this._sourceItems;
    }

    public set sourceItems(value: any[]) {
        this._sourceItems = value;
    }

    @Input() public get secondarySourceItems(): any[] {
        return this._secondarySourceItems;
    }

    public set secondarySourceItems(value: any[]) {
        this._secondarySourceItems = value;
    }

    @Input() public get title(): string {
        return this._title;
    }

    public set title(value: string) {
        this._title = value;
    }

    @Input() public get description(): string {
        return this._description;
    }

    public set description(value: string) {
        this._description = value;
    }

    @Input() public get reference(): string {
        return this._reference;
    }

    public set reference(value: string) {
        this._reference = value;
    }

    @Input() public get action(): string {
        return this._action;
    }

    public set action(value: string) {
        this._action = value;
    }

    @Input() public get function(): OperatorFunction<any, any> {
        return this._function;
    }

    public set function(value: OperatorFunction<any, any>) {
        this._function = value;
    }

    public get results(): any[] {
        return this._results;
    }

    public set results(value: any[]) {
        this._results = value;
    }

    // Constructor ********************************************************************************
    constructor() {

    }

    // Interface Methods **************************************************************************
    public ngOnDestroy(): void {
        if (this._subscription !== undefined) {
            this._subscription.unsubscribe();
            this._subscription = undefined;
        }
    }

    // Private Methods ****************************************************************************
    private execute(delay: number): void {
        const result$ = this.getResults(delay);
        this._subscription = result$.subscribe(x => this.results.push(x));
    }

    private getResults(delay: number): Observable<any> {
        if (this._secondarySourceItems === undefined || this._secondarySourceItems.length === 0) {
            return this.executeSingleSource(delay);
        }

        return this.executeMultipleSource(delay);
    }

    private executeSingleSource(delay: number): Observable<any> {
        this.clear();

        const source$ = from(this._sourceItems).pipe(zip(interval(delay), x => x));
        return source$.pipe(
            this.function
        );
    }

    private executeMultipleSource(delay: number): Observable<any> {
        this.clear();

        // const psource$ = from(this._sourceItems).pipe(zip(interval(delay), x => x));
        // const ssource$ = from(this._secondarySourceItems).pipe(zip(interval(delay), x => x));

        // const r = psource$.pipe(switchMap(x => ssource$));
        const psource$ = from(this._sourceItems);
        // const ssource$ = from(this._secondarySourceItems);

        const r = psource$.pipe(this.function);

        return r;
    }

    private clear(): void {
        this.results = [];
    }

    // Event Handlers *****************************************************************************
    public handleExecuteClicked(event: MouseEvent): void {
        this.execute(0);
    }

    public handleSlowItDownClicked(event: MouseEvent): void {
        this.execute(1000);
    }

    public handleClearClicked(event: MouseEvent): void {
        this.results = [];
    }
}
