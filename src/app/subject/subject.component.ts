import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-subject',
    templateUrl: './subject.component.html',
    styleUrls: [ './subject.component.scss' ],
    animations: [ fadeInAnimation ]
})
export class SubjectComponent {

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }
}
