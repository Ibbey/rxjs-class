import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-patterns',
    templateUrl: './patterns.component.html',
    styleUrls: [ './patterns.component.scss' ],
    animations: [ fadeInAnimation ]
})
export class PatternsComponent {

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }
}
