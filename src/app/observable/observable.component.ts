import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-observable',
    templateUrl: './observable.component.html',
    styleUrls: [ './observable.component.scss' ],
    animations: [ fadeInAnimation ]
})
export class ObservableComponent {

    constructor() {

    }

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }
}
