import { trigger, transition, style, animate, query, group } from '@angular/animations';

export const fadeInAnimation = trigger('routeAnimations', [
    transition('* => *', [
        query(':enter', [
            style({ opacity: 0})
        ], { optional: true }),
        group([
            query(':leave', [
                animate(300, style({ opacity: 0 }))
            ], { optional: true }),
            query(':enter', [
                style({ opacity: 0 }),
                animate(300, style({ opacity: 1 }))
            ], { optional: true })
        ])
    ])
]);
