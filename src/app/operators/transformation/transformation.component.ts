import { Component, HostBinding } from '@angular/core';
import { OperatorFunction, of } from 'rxjs';
import { map, switchMap, filter, find, count, distinct, reduce } from 'rxjs/operators';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-transformation',
    templateUrl: './transformation.component.html',
    styleUrls: [ './transformation.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class TransformationComponent {
    // Member Variables ***************************************************************************
    private _numericItems: number[];
    private _otherNumericItems: number[];
    private _indistinctList: number[];

    private _mapFunction: OperatorFunction<{}, any>;
    private _switchMapFunction: OperatorFunction<{}, any>;

    private _filterFunction: OperatorFunction<any, any>;
    private _findFunction: OperatorFunction<any, any>;
    private _distinctFunction: OperatorFunction<any, any>;

    private _countFunction: OperatorFunction<any, any>;
    private _reduceFunction: OperatorFunction<any, any>;

    // Public Properties **************************************************************************
    public get numericItems(): number[] {
        return this._numericItems;
    }

    public get otherNumericItems(): number[] {
        return this._otherNumericItems;
    }

    public get indistinctList(): number[] {
        return this._indistinctList;
    }

    public get mapFunction(): OperatorFunction<any, any> {
        return this._mapFunction;
    }

    public get switchMapFunction(): OperatorFunction<any, any> {
        return this._switchMapFunction;
    }

    public get filterFunction(): OperatorFunction<any, any> {
        return this._filterFunction;
    }

    public get findFunction(): OperatorFunction<any, any> {
        return this._findFunction;
    }

    public get distinctFunction(): OperatorFunction<any, any> {
        return this._distinctFunction;
    }

    public get countFunction(): OperatorFunction<any, any> {
        return this._countFunction;
    }

    public get reduceFunction(): OperatorFunction<any, any> {
        return this._reduceFunction;
    }

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._numericItems = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        this._otherNumericItems = [ 10, 10, 10 ];
        this._indistinctList = [ 1, 1, 1, 2, 3, 3, 4, 4, 5, 5 ];


        this._mapFunction = map((x: number) => x * 5);
        this._switchMapFunction = switchMap((x: number) => of(x * 10));
        this._filterFunction = filter((x: number) => x % 2 === 0);
        this._findFunction = find((x: number) => x % 5 === 0);
        this._distinctFunction = distinct();
        this._countFunction = count();
        this._reduceFunction = reduce((x, y) => x + y);
    }
}
