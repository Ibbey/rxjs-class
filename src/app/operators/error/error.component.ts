import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-error',
    templateUrl: './error.component.html',
    styleUrls: [ './error.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class ErrorComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************


    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }
}
