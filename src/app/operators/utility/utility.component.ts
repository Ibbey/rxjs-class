import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-utility',
    templateUrl: './utility.component.html',
    styleUrls: [ './utility.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class UtilityComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************


    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }
}
