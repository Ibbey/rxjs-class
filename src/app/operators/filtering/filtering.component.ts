import { Component, HostBinding } from '@angular/core';
import { OperatorFunction, of, timer } from 'rxjs';
import { map, switchMap, filter, find, first, last, count, distinct, debounce, skip, takeLast, takeUntil, takeWhile } from 'rxjs/operators';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-filtering',
    templateUrl: './filtering.component.html',
    styleUrls: [ './filtering.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class FilteringComponent {
    // Member Variables ***************************************************************************
    private _numericItems: number[];
    private _otherNumericItems: number[];
    private _indistinctList: number[];

    private _mapFunction: OperatorFunction<{}, any>;
    private _switchMapFunction: OperatorFunction<{}, any>;

    private _filterFunction: OperatorFunction<any, any>;
    private _findFunction: OperatorFunction<any, any>;
    private _firstFunction: OperatorFunction<any, any>;
    private _lastFunction: OperatorFunction<any, any>;
    private _distinctFunction: OperatorFunction<any, any>;
    private _debounceFuncion: OperatorFunction<any, any>;
    private _skipFunction: OperatorFunction<any, any>;
    private _takeLastFunction: OperatorFunction<any, any>;
    private _takeWhileFunction: OperatorFunction<any, any>;
    private _takeUnilFunction: OperatorFunction<any, any>;

    private _countFunction: OperatorFunction<any, any>;

    // Public Properties **************************************************************************
    public get numericItems(): number[] {
        return this._numericItems;
    }

    public get otherNumericItems(): number[] {
        return this._otherNumericItems;
    }

    public get indistinctList(): number[] {
        return this._indistinctList;
    }

    public get mapFunction(): OperatorFunction<any, any> {
        return this._mapFunction;
    }

    public get switchMapFunction(): OperatorFunction<any, any> {
        return this._switchMapFunction;
    }

    public get filterFunction(): OperatorFunction<any, any> {
        return this._filterFunction;
    }

    public get findFunction(): OperatorFunction<any, any> {
        return this._findFunction;
    }

    public get firstFunction(): OperatorFunction<any, any> {
        return this._firstFunction;
    }

    public get lastFunction(): OperatorFunction<any, any> {
        return this._lastFunction;
    }

    public get distinctFunction(): OperatorFunction<any, any> {
        return this._distinctFunction;
    }

    public get countFunction(): OperatorFunction<any, any> {
        return this._countFunction;
    }

    public get debounceFunction(): OperatorFunction<any, any> {
        return this._debounceFuncion;
    }

    public get skipFunction(): OperatorFunction<any, any> {
        return this._skipFunction;
    }

    public get takeLastFunction(): OperatorFunction<any, any> {
        return this._takeLastFunction;
    }

    public get takeWhileFunction(): OperatorFunction<any, any> {
        return this._takeWhileFunction;
    }

    public get takeUntilFunction(): OperatorFunction<any, any> {
        return this._takeUnilFunction;
    }

    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {
        this._numericItems = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        this._otherNumericItems = [ 10, 10, 10 ];
        this._indistinctList = [ 1, 1, 1, 2, 3, 3, 4, 4, 5, 5 ];


        this._mapFunction = map((x: number) => x * 5);
        this._switchMapFunction = switchMap((x: number) => of(x * 10));
        this._filterFunction = filter((x: number) => x % 2 === 0);
        this._findFunction = find((x: number) => x % 5 === 0);
        this._firstFunction = first((x: number) => x % 5 === 0);
        this._lastFunction = last((x: number) => x % 5 === 0);
        this._distinctFunction = distinct();
        this._countFunction = count();
        this._debounceFuncion = debounce(x => timer(1000));
        this._skipFunction = skip(3);
        this._takeLastFunction = takeLast(4);
        this._takeUnilFunction = takeUntil(timer(1000));
        this._takeWhileFunction = takeWhile(x => x < 6);
    }
}
