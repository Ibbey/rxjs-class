import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-multi-casting',
    templateUrl: './multi-casting.component.html',
    styleUrls: [ './multi-casting.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class MultiCastingComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************


    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }
}
