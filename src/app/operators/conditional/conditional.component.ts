import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-conditional',
    templateUrl: './conditional.component.html',
    styleUrls: [ './conditional.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class ConditionalComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************


    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }
}
