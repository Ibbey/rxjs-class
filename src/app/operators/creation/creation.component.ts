import { Component, HostBinding } from '@angular/core';

import { fadeInAnimation } from '../../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-operators-creation',
    templateUrl: './creation.component.html',
    styleUrls: [ './creation.component.scss' ],
    animations: [
        fadeInAnimation
     ]
})
export class CreationComponent {
    // Member Variables ***************************************************************************

    // Public Properties **************************************************************************


    @HostBinding('@routeAnimations') get fadeIn() {
        return '';
    }

    // Constructor ********************************************************************************
    constructor() {
        this.initialize();
    }

    // Private Methods ****************************************************************************
    private initialize(): void {

    }
}
