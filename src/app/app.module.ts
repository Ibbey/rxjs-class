// Angular Imports ********************************************************************************
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

// Angular Material Imports ***********************************************************************
import { MatButtonModule, MatButtonToggleModule, MatSidenavModule, MatCheckboxModule, MatToolbarModule,
         MatIconModule, MatSlideToggleModule, MatInputModule, MatFormFieldModule, MatSelectModule, MatTableModule,
         MatExpansionModule, MatCardModule, MatListModule, MatTooltipModule, MatGridListModule, MatDatepickerModule,
         MatNativeDateModule, MatDividerModule, MatTreeModule, MatMenuModule, MatProgressSpinnerModule  } from '@angular/material';

// Application Routing Imports ********************************************************************
import { RXJS_CLASS_ROUTE_MODULE } from './app.routes';


// Application Components Imports *****************************************************************
import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { HomeComponent } from './home/home.component';
import { PatternsComponent } from './patterns/patterns.component';
import { OperatorsComponent } from './operators/operators.component';
import { ValueBlockComponent } from './value-block/value-block.component';
import { OperatorBlockComponent } from './operator-block/operator-block.component';
import { TransformationComponent } from './operators/transformation/transformation.component';
import { FilteringComponent } from './operators/filtering/filtering.component';
import { ObservableComponent } from './observable/observable.component';
import { SubjectComponent } from './subject/subject.component';
import { CombinationComponent } from './operators/combination/combination.component';
import { ConditionalComponent } from './operators/conditional/conditional.component';
import { CreationComponent } from './operators/creation/creation.component';
import { ErrorComponent } from './operators/error/error.component';
import { MultiCastingComponent } from './operators/multi-casting/multi-casting.component';
import { UtilityComponent } from './operators/utility/utility.component';

import { HighlightModule } from 'ngx-highlightjs';
import typescript from 'highlight.js/lib/languages/typescript';

export function hljsLanguages() {
    return [
      {name: 'typescript', func: typescript},
    ];
  }

@NgModule({
    imports: [
        HighlightModule.forRoot({
            languages: hljsLanguages
          }),
        FlexLayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RXJS_CLASS_ROUTE_MODULE,
        MatButtonModule,
        MatButtonToggleModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatIconModule,
        MatSlideToggleModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatTableModule,
        MatExpansionModule,
        MatCardModule,
        MatListModule,
        MatTooltipModule,
        MatGridListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatDividerModule,
        MatTreeModule,
        MatMenuModule,
        MatProgressSpinnerModule
    ],
    declarations: [
        BootstrapComponent,
        HomeComponent,
        PatternsComponent,
        OperatorsComponent,
        ValueBlockComponent,
        OperatorBlockComponent,
        TransformationComponent,
        FilteringComponent,
        ObservableComponent,
        SubjectComponent,
        CombinationComponent,
        ConditionalComponent,
        CreationComponent,
        ErrorComponent,
        MultiCastingComponent,
        UtilityComponent
    ],
    providers: [

    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ],
    bootstrap: [
        BootstrapComponent
    ]
})
export class AppModule { }
