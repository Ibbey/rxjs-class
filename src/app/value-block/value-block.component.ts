import { Component, Input } from '@angular/core';

@Component({
    selector: 'rxt-value-block',
    templateUrl: './value-block.component.html',
    styleUrls: [ './value-block.component.scss' ]
})
export class ValueBlockComponent {
    // Member Variables ***************************************************************************
    private _value: number;

    // Public Properties **************************************************************************
    @Input() public get value(): number {
        return this._value;
    }

    public set value(value: number) {
        this._value = value;
    }

    // Constructor ********************************************************************************
    constructor() {

    }
}
