import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { PatternsComponent } from './patterns/patterns.component';
import { OperatorsComponent } from './operators/operators.component';
import { TransformationComponent } from './operators/transformation/transformation.component';
import { FilteringComponent } from './operators/filtering/filtering.component';
import { ObservableComponent } from './observable/observable.component';
import { SubjectComponent } from './subject/subject.component';
import { CombinationComponent } from './operators/combination/combination.component';
import { ConditionalComponent } from './operators/conditional/conditional.component';
import { CreationComponent } from './operators/creation/creation.component';
import { ErrorComponent } from './operators/error/error.component';
import { MultiCastingComponent } from './operators/multi-casting/multi-casting.component';
import { UtilityComponent } from './operators/utility/utility.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: 'home',
        pathMatch: 'full',
        component: HomeComponent,
        data: { animation: 'HomePage' }
    },
    {
        path: 'patterns',
        pathMatch: 'full',
        component: PatternsComponent,
        data: { animation: 'PatternsPage' }
    },
    {
        path: 'operators',
        pathMatch: 'full',
        component: OperatorsComponent,
        data: { animation: 'OperatorsPage' }
    },
    {
        path: 'transformation',
        pathMatch: 'full',
        component: TransformationComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'filtering',
        pathMatch: 'full',
        component: FilteringComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'observable',
        pathMatch: 'full',
        component: ObservableComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'subject',
        pathMatch: 'full',
        component: SubjectComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'combination',
        pathMatch: 'full',
        component: CombinationComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'conditional',
        pathMatch: 'full',
        component: ConditionalComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'creation',
        pathMatch: 'full',
        component: CreationComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'error-handling',
        pathMatch: 'full',
        component: ErrorComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'multicasting',
        pathMatch: 'full',
        component: MultiCastingComponent,
        data: { animation: 'OperatersPage'}
    },
    {
        path: 'utility',
        pathMatch: 'full',
        component: UtilityComponent,
        data: { animation: 'OperatersPage'}
    }
];

export const RXJS_CLASS_ROUTE_MODULE: ModuleWithProviders = RouterModule.forRoot(routes);
