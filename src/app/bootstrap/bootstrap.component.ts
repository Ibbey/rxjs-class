import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

import { fadeInAnimation } from '../common/animations/fade-in.animation';

@Component({
    selector: 'rxt-bootstrap',
    templateUrl: './bootstrap.component.html',
    styleUrls: [ './bootstrap.component.scss' ],
    animations: [ fadeInAnimation ]
})
export class BootstrapComponent {
    // Member Variables ***************************************************************************
    private readonly _router: Router;

    // Constructor ********************************************************************************
    constructor(router: Router) {
        this._router = router;
    }

    // Event Handlers *****************************************************************************
    public prepareRoute(outlet: RouterOutlet) {
        return outlet.activatedRouteData.animation;
    }

    public handleHomeClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/home');
    }

    public handlePatternsClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/patterns');
    }

    public handleOperatorsClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/operators');
    }

    public handleTransformationClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/transformation');
    }

    public handleFilteringClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/filtering');
    }

    public handleMathematicalClicked(event: MouseEvent): void {
        this._router.navigateByUrl('/mathematical');
    }
}
