debounce
distinct
delay
switchMap / concatMap
mergeMap
map
catchError
withLatestFrom

filter
find
last
first